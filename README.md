# PuurIDee CoCo CMS Template Boilerplate

A boilerplate template for CoCo CMS

This boilerplate template serves as a starting point for CoCo based websites. It is a Bootstrap 3 skeleton layout with a basic navbar view available in the layouts/partials folder.

## Getting started
Fork this repository and update all boilerplate references. Then register it in CoCo CMS by adding the new repository to composer in your Laravel app running CoCo

Make sure to update the local ```composer.json``` file's name and autoload attributes and the namespace and $packageName attribute in ```src/TemplateServiceProvider.php```, set them to the project's name.

> composer.json
> line 2: "name": "coco/```boilerplate```-template", // eg. ```"name": "coco/puuridee-template"```
> line 21: "CoCo\\\\```Boilerplate```Template\\\\": "src/" // eg. ```"CoCo\\PuurideeTemplate\\": "src/"```

>TemplateServiceProvider.php
> line 1:  <?php namespace CoCo\\```Boilerplate```Template; // eg. ```<?php namespace CoCo\PuurideeTemplate;```
> line 11: protected $packageName = '```boilerplatetemplate```'; // eg. ```protected $packageName = 'puuridee';```

## Usage

### SASS

The boilerplate is setup to compile and compress css from scss to the package folder and the Laravel public css folder:
> scss-file: src/ssss/main.scss   
> Package: src/assets/css/screen.css   
> Public: public/theme/{theme-name}/css/screen.css   

Make sure to have sass installed: http://sass-lang.com/install
> ```$ gem install sass``` on macOS

## Laravel Mix

A Laravel Mix config file for Webpack is available in the _tools/laravel\_mix/webpack.mix.js_ file, copy this file to your Laravel projects root directory and run ``` $ npm install``` once and ```$ npm run watch``` while editing template sass files, to minimize css use ```$ npm run production```.

