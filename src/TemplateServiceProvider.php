<?php namespace CoCo\butiq;

use Illuminate\Support\ServiceProvider;

/**
 * A default template package for PuurIDee CoCo CMS
 *
 */
class TemplateServiceProvider extends ServiceProvider {

    protected $packageName = 'butiq';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->handleTranslations();
        $this->handleViews();
        $this->handleAssets();
    }

    /**
     * Translation files
     */
    private function handleTranslations()
    {
        // @TODO Needs to be done
    }

    /**
     * View files.
     */
    private function handleViews()
    {
        // Register Views
        $this->loadViewsFrom(__DIR__.'/views', $this->packageName);
    }

    /**
     * Public asset files.
     */
    private function handleAssets()
    {
        // Register the asset's publisher
        $this->publishes([
            __DIR__ . '/assets' => public_path('/theme/' . $this->packageName),
        ], 'public');
    }
}
