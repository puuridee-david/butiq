<div class="contact-form-block-type-1 row" style="background-color: {{seasonColor($block->get('background-color'))}};
        padding-top: {{getMargin($block->get('block-margin'))}};
        padding-bottom: {{getMargin($block->get('block-margin'))}};">
    <div class="col-md-8 col-md-push-2 col-sm-10 col-sm-push-1 col-xs-10 col-xs-push-1">
        <div class="row">
            <div class="col-md-12" data-aos="fade-up">
                <h1 style="color: {{seasonColor($block->get('title-color'))}};">
                    {{$block->get('title')}}
                </h1>
                {!! $block->get('content') !!}
            </div>
            @if(CoCo::trackingAllowed())
                <div class="col-md-12">
                    @if (\Session::has('success'))
                        <p style="color: green">{!! \Session::get('success') !!}</p>
                    @endif
                    <form class="" action="/contactform/send" name="Contactformulier" method="post" id="form">
                        {{ csrf_field() }}

                        <input type="hidden" name="success_response_message"
                               value="Bedankt! We hebben je aanvraag ontvangen.">

                        <label for="name">Naam<br>
                            <input type="text" name="Naam" id="name" placeholder="Naam" required>
                        </label>
                        <br>

                        <label for="phone">Telefoon <br>
                            <input type="tel" name="Telefoonnummer" id="phone" placeholder="Telefoonnummer">
                        </label>
                        <br>

                        <label for="email">Email<br>
                            <input type="email" name="email" id="email" placeholder="E-mailadres" required>
                        </label>
                        <br>

                        <div class="checkboxes">
                            <label for="appointment"><input type="checkbox" name="Afspraak" id="appointment"
                                                            @if(isset($_GET['afspraak'])) checked @endif/><span>Ik wil een afspraak maken</span></label>
                        </div>

                        <div id="appointment-details" @if(!isset($_GET['afspraak'])) hidden @endif>
                            <div class="appointment-details-block">
                                <label>Voorkeurs dag(en)</label><br>
                                <div class="checkboxes">
                                    <label><input type="checkbox" name="Dinsdag" id=""/><span>Dinsdag</span></label>
                                    <label><input type="checkbox" name="Woensdag" id=""/><span>Woensdag</span></label>
                                    <label><input type="checkbox" name="Donderdag" id=""/><span>Donderdag</span></label>
                                    <label><input type="checkbox" name="Vrijdag" id=""/><span>Vrijdag</span></label>
                                    <label><input type="checkbox" name="Zaterdag" id=""/><span>Zaterdag</span></label>
                                </div>
                            </div>

                            <div class="appointment-details-block">
                                <label>Op welk moment van de dag?</label><br>
                                <div class="checkboxes">
                                    <label><input type="checkbox" name="Ochtend" id=""/><span>'s ochtends</span></label>
                                    <label><input type="checkbox" name="Middag" id=""/><span>'s middags</span></label>
                                </div>
                            </div>

                            <div class="appointment-details-block">
                                <p class="gray">We nemen contact met jou op om de afspraak definitief te maken.</p>
                            </div>
                        </div>

                        <label for="comment">Vragen en/of opmerkingen<br>
                            <textarea name="Vragen/Opmerkingen" id="comment" cols="30" rows="10" required></textarea>
                        </label>

                        <div
                                id='recaptcha'
                                class="g-recaptcha"
                                data-sitekey="{{ CoCo::getSetting('app.recaptcha.site_key') }}"
                                data-callback="submitForm"
                                data-size="invisible"></div>
                        <button
                                data-validate-form="#form" class="button-arrow" type="button"
                                style="color:{{seasonColor($block->get('button-text-color'))}}; background-color: {{seasonColor($block->get('button-background-color'))}};"
                                onclick="ga('send', 'event', 'Contact', 'Afspraak maken', 'click');">
                            Versturen <i class="fa fa-angle-right fa-2x"></i>
                        </button>
                    </form>
                </div>
            @endif
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $('#appointment').click(function () {
            if (document.getElementById('appointment').checked) {
                $("#appointment-details").fadeIn();
            } else {
                $("#appointment-details").fadeOut();
            }
        });
    </script>
@endpush
@push('scripts')
    <script src="{{ asset('js/validate-form.js') }}"></script>
@endpush