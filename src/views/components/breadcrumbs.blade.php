<div class="row breadcrumbs-type-1">
    @if (isset($page) && $page->viewName != 'homepage')
        <ol class="breadcrumb">
            <li><a href="/" style="color:{{seasonColors()->get('primary')}}">Home</a></li>
            @foreach ($page->path as $parent)
                <li>
                    <a href="{{ $parent->url }}" style="color:{{seasonColors()->get('primary')}}">{{ $parent->name }}</a>
                </li>
            @endforeach
            <li class="active" style="color:{{seasonColors()->get('secondary')}}">{{ $page->get('name') }}</li>
        </ol>
    @endif
</div>