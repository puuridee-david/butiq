<div class="text-image-or-video-block-type-1 row"
     style="background-color: {{seasonColor($block->get('background-color'))}};
             padding-top: {{getMargin($block->get('block-margin'))}};
             padding-bottom: {{getMargin($block->get('block-margin'))}};" data-aos="fade-left">
    <div class="col-md-6">
        @if(blockAlignment($block) == 'media-left')
            <div class="image-or-video-content-left">
                @include(CoCo::getSetting('app.template_package') . '::components.partials.image-or-video-partial', ['block' => $block])
            </div>
        @else
            <div class="title-content-left">
                @include(CoCo::getSetting('app.template_package') . '::components.partials.title-content-partial', ['block' => $block])
            </div>
        @endif
    </div>
    <div class="col-md-6" data-aos="fade-right">
        @if(blockAlignment($block) == 'media-left')
            <div class="title-content-right">
                @include(CoCo::getSetting('app.template_package') . '::components.partials.title-content-partial', ['block' => $block])
            </div>
        @else
            <div class="image-or-video-content-right">
                @include(CoCo::getSetting('app.template_package') . '::components.partials.image-or-video-partial', ['block' => $block])
            </div>
        @endif
    </div>
</div>