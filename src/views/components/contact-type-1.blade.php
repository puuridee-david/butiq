<div class="container contact-type-1">
    <div class="row">
        <div class="col col-md-6">
            <h1 class="contact-type-1__header">
                @if (isset($_GET['afspraak']))
                    Samen een afspraak maken
                @else
                    Stuur ons een bericht
                @endif
            </h1>
            <p class="contact-type-1__content">

            </p>
        </div>
        <div class="col col-md-6">
            @if (\Session::has('success'))
                <div class="alert blue-background alert-dismissable m-t-10">
                    <a href="#" class="close m-r-10" data-dismiss="alert" aria-label="close">&times;</a>
                    <p class="text-white">{!! \Session::get('success') !!}</p>
                </div>
            @endif
            <form class="contact-type-1__form" action="/contactform/send" name="Contactformulier" method="post"
                  id="form">
                {{ csrf_field() }}
                <label for="name">Naam<br>
                    <input type="text" name="Naam" id="name" placeholder="Naam" required>
                </label><br>
                <label for="phone">Telefoonnummer <br>
                    <input type="tel" name="Telefoonnummer" id="phone" placeholder="Telefoonnummer">
                </label><br>
                <label for="email">Email<br>
                    <input type="email" name="email" id="email" placeholder="E-mailadres" required>
                </label><br>
                @if (isset($_GET['afspraak']))
                    {{-- Voorkeursdagen --}}
                    {{-- Voorkeurtijdstip --}}
                @endif
                <label for="comment">
                    <textarea name="Vragen/Opmerkingen" id="comment" cols="30" rows="10" required></textarea>
                </label>
                <input type="hidden" name="success_response_message" value="Bedankt! We hebben je aanvraag ontvangen.">
                <div
                        id='recaptcha'
                        class="g-recaptcha"
                        data-sitekey="{{ CoCo::getSetting('app.recaptcha.site_key') }}"
                        data-callback="submitForm"
                        data-size="invisible"></div>
                <button type="button"
                        data-validate-form="#form" class="contact-type-1__button button-type-1">
                    Versturen
                    <i class="fa fa-angle-right fa-2x"></i>
                </button>
            </form>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        $('#form').submit(function () {
            var email = $('#email').val();
            return true;
        });
    </script>
@endpush
@if(CoCo::trackingAllowed())
    @push('scripts')
        <script src="{{ asset('js/validate-form.js') }}"></script>
    @endpush
@endif