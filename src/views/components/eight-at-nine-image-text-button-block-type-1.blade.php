<div class="eight-at-nine-image-text-button-block-type-1 row" data-aos-easing="ease-in-sine">

    <div class="image {{blockAlignmentClass($block)}}"
         data-background-image="{{getFirstImageOrPlaceholder($block, 960, 1080)}}"
         style="background: url('{{ getFirstImageOrPlaceholder($block, 960, 1080) }}') no-repeat center center;
                 background-size: cover;
                 " data-aos="fade-up"></div>

    <div class="block-container {{blockAlignmentClass($block)}}"
         style="background-color: {{seasonColor($block->get('background-color'))}}" data-aos="fade-up">
        <div class="content-container">
            <div class="content">
                <div class="content-text inhert-link" style="color: {{seasonColor($block->get('text-color'))}};">
                    {!! $block->get('content') !!}
                    <div class="button-link"
                         style="background-color: {{seasonColor($block->get('button-background-color'))}};">
                        <a href="{{$block->get('url','#')}}"
                           style="color:{{seasonColor($block->get('button-text-color'))}}!important;"
                           target="{{getLinkTarget($block->get('url-target'))}}">{{$block->get('button-name')}} <i
                                    class="fa fa-angle-right"></i></a>
                    </div>
                    @if(strlen($block->get('lower-url')) && strlen($block->get('lower-url-name')))
                        <div class="link-text">
                            <a href="{{$block->get('lower-url')}}" target="{{getLinkTarget($block->get('lower-url-target'))}}"
                               onclick="ga('send', 'event', '{{url()->current()}}','{{$block->get('lower-url-name')}}', 'click');"
                            >{{$block->get('lower-url-name')}}</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>