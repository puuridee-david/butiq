<?php
$footer = CoCo::getPage(9);
?>

<div class="eight-at-nine-maps-contact-block-type-1 row" data-aos-easing="ease-in-sine">
    <div class="col col-md-6 col-xs-12 col-sm-12" data-aos="fade-right">
        @if(CoCo::trackingAllowed())
            <iframe class="google_maps"
                    src="https://www.google.com/maps/embed?pb=!4v1517915738432!6m8!1m7!1sCAoSLEFGMVFpcE8wTDVaY3pHWDBsVElnLWcwZzEweVJ3TmlJRlJUODdrTVZaWjla!2m2!1d53.17577225054611!2d6.972313150763512!3f60.19212159808728!4f-4.308110951538026!5f0.7820865974627469"
                    width="100%" height="500px" frameborder="0" style="border:0; margin-bottom: -20px;"></iframe>
        @endif
    </div>
    <div class="col col-md-6 col-xs-12 col-sm-12" data-aos="fade-left">
        <h1 class="eight-at-nine-maps-contact-block-type-1__header" style="color: {{seasonColors()->get('primary') }};">
            Butiq - Optiek met stijl
        </h1>
        <a class="eight-at-nine-maps-contact-block-type-1__link"
           href="https://www.google.nl/maps/place/{{ $footer->get('streetname') }} {{ $footer->get('housenumber') }}, {{ $footer->get('zip') }} {{ $footer->get('town') }}/"
           target="_blank"> {{ $footer->get('streetname') }} {{ $footer->get('housenumber') }}
            , {{ $footer->get('zip') }} {{ $footer->get('town') }}</a><br>
        <a class="eight-at-nine-maps-contact-block-type-1__link" href="tel:{{ $footer->get('phone-number') }}"
           onclick="ga('send', 'event', '{{url()->current()}}','{{ $footer->get('phone-number') }}', 'click');"> {{ $footer->get('phone-number') }}</a><br>
        <a class="eight-at-nine-maps-contact-block-type-1__link" href="mailto:{{ $footer->get('e-mail') }}"
           onclick="ga('send', 'event', '{{url()->current()}}','{{ $footer->get('e-mail') }}', 'click');"> {{ $footer->get('e-mail') }}</a><br>
        <h1 class="eight-at-nine-maps-contact-block-type-1__header"
            style="color: {{seasonColors()->get('primary') }};margin-left: 2px;">
            Openingstijden
        </h1>
        @foreach($footer->get('opening-hours') as $item => $value)
            <span class="feight-at-nine-maps-contact-block-type-1__opening-time col col-md-12 col-xs-12 col-sm-12"
                  style="margin-left: -10px;">
                    {{ $value }}
                </span>
        @endforeach
    </div>
</div>