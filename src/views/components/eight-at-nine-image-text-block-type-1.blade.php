<div class="eight-at-nine-image-text-block-type-1 row" data-aos-easing="ease-in-sine">

    <div class="image {{blockAlignmentClass($block)}}" data-background-image="{{getFirstImageOrPlaceholder($block, 960, 1080)}}"
         style="background: url('{{ getFirstImageOrPlaceholder($block, 960, 1080) }}') no-repeat center center;
                 background-size: cover;
                 " data-aos="fade-right"></div>

    <div class="block-container {{blockAlignmentClass($block)}}" style="background-color: {{seasonColor($block->get('background-color'))}}" data-aos="fade-left">
        <div class="content-container">
            <div class="content">
                <div class="content-text chevron-link inhert-link" style="color: {{seasonColor($block->get('text-color'))}};">
                    {!! $block->get('content') !!}
                </div>
            </div>
        </div>
    </div>

</div>