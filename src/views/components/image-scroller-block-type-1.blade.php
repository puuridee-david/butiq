<?php
$brands = CoCo::getPage(25);
?>
<div class="image-scroller-block-type-1 row" style="background-color: {{seasonColor($block->get('background-color'))}}" data-aos="fade-right" data-aos-easing="ease-in-sine">
    <div class="images-slider">
        @foreach($brands->content as $brand)
            <a href="{{$brand->get('url')}}" class="no-style item"
               target="{{getLinkTarget($brand->get('url-target'))}}">
                <img class="img-responsive" src="{{getFirstImageOrPlaceholder($brand, 350, 160)}}" style="width: 100%;">
            </a>
        @endforeach
    </div>
</div>
@push('scripts')
    <script>
        $('.images-slider').slick({
            arrows: true,
            autoplay: true,
            autoplaySpeed: 10000,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            prevArrow: '<button type="button" class="slick-prev "></button>',
            nextArrow: '<button type="button" class="slick-next "></button>',
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    </script>
@endpush