<div class="row two-colors-background-center-image-block-type-1" data-aos="fade-up"
     style="height: {{setHeight($block->get('block-height','100vh'))}}">

    <div class="left-block" style="background-color: {{seasonColors()->get('primary-tint')}}"></div>
    <div class="right-block" style="background-color: {{seasonColors()->get('primary')}}"></div>

    <div class="left-button"
         style="background-color: {{seasonColors()->get('primary')}}">
        <a href="{{$block->get('left-button-url','#')}}" style="color:{{seasonColors()->get('blank')}}"
           target="{{getLinkTarget($block->get('left-button-target'))}}">{{$block->get('left-button-name')}} <i
                    class="fa fa-angle-right"
                    onclick="ga('send', 'event', '{{url()->current()}}','{{$block->get('left-button-name')}}', 'click');"></i></a>
    </div>

    <div class="right-button"
         style="background-color: {{seasonColors()->get('blank')}}">
        <a href="{{$block->get('right-button-url','#')}}" style="color:{{seasonColors()->get('primary')}}"
           target="{{getLinkTarget($block->get('right-button-target'))}}">{{$block->get('right-button-name')}} <i
                    class="fa fa-angle-right"
                    onclick="ga('send', 'event', '{{url()->current()}}','{{$block->get('right-button-name')}}', 'click');"></i></a>
    </div>

    <div class="img-container">
        <img data-aos="fade-in" data-aos-delay="500" src="{{getSeasonImageOrDefault($block)}}">
    </div>

    <div class="title-block"><h1>{{$block->get('title')}}</h1></div>
    @if($index == 0)
        <div class="scroll-down-button" hidden>
            <i class="fa fa-angle-down"></i>
        </div>
    @endif
</div>
@push("scripts")
    <script>

        $(window).resize(function () {
            alignScrolDownButton();
        });
        $(document).ready(function () {
            alignScrolDownButton();
        });

        $(".scroll-down-button").click(function () {
            $('html, body').animate({scrollTop: $(window).height() + $(".navbar").height() + $(".usp").height()}, 1500);
        });

        $(window).scroll(function () {
            $(".scroll-down-button").css("opacity", 1 - $(window).scrollTop() / 250);
        });

        function alignScrolDownButton() {
            var windowHeight = $(window).height();
            var navigationBarHeight = $(".navbar").height() + $(".usp").height();
            var elementHeight = $(".scroll-down-button").height();
            height = windowHeight - navigationBarHeight - elementHeight - elementHeight - 25;
            $(".scroll-down-button").css({top: height, left: 0, position: 'absolute', display: 'flex'});
        }

    </script>
@endpush