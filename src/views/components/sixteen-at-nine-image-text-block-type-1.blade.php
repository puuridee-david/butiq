<style>
    .chevron-link a:hover {
        color: {{seasonColor("Primaire kleur")}};
    }
</style>
<div class="sixteen-at-nine-image-text-block-type-1 row">

    <div class="image {{blockAlignmentClass($block)}}" data-aos="fade-left"
         data-background-image="{{getFirstImageOrPlaceholder($block, 1920, 1080)}}"
         style="background: url('{{ getFirstImageOrPlaceholder($block, 1920, 1080) }}') no-repeat center center;
                 background-size: cover;
                 "></div>

    <div class="block-container {{blockAlignmentClass($block)}}" data-aos="fade-right">
        <div class="content-container"
             style="background-color: {{seasonColor($block->get('background-color'))}}; text-align: {{setAlignment($block->get('content-align'))}}">
            <div class="content">
                <div class="content-text chevron-link inhert-link"
                     style="color: {{seasonColor($block->get('text-color'))}};">
                    {!! $block->get('content') !!}
                </div>
            </div>
        </div>
    </div>
</div>