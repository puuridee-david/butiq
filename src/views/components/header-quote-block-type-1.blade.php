<div class="header-quote-block-type-1 row"
           data-background-image="{{getFirstImageOrPlaceholder($block, 1920, 1080)}}"
           style="background: url('{{ getFirstImageOrPlaceholder($block, 1920, 1080) }}') no-repeat center center;
                   background-size: cover;
                   height: {{setHeight($block->get('block-height','100vh'))}};
                   " data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-anchor-placement="top-center">

    <div class="header-quote-content" data-aos="fade-down" data-aos-easing="ease-in-sine">
        <h1 class="shadow"
            style="text-align: {{setAlignment($block->get('title-align'))}}">{{$block->get('title')}}</h1>
        <div class="shadow" style="text-align: {{setAlignment($block->get('content-align'))}}">
            {!! $block->get('content') !!}
        </div>
    </div>
</div>