<h1 style="color: {{seasonColor($block->get('title-color'))}}; text-align: {{setAlignment($block->get('title-align'))}}">
    {{$block->get('title')}}
</h1>
<div class="chevron-link inhert-link"
     style="color: {{seasonColor($block->get('text-color'))}}; text-align: {{setAlignment($block->get('content-align'))}};">
    {!! $block->get('content') !!}
</div>
