@if(strlen($block->get('youtube-url')))
    @if(CoCo::trackingAllowed())
        <div class="youtube-video-container">
            <iframe title="YouTube video player" class="youtube-player" type="text/html"
                    width="100%" height="500px" src="{{str_replace('watch?v=', 'embed/', $block->get('youtube-url'))}}"
                    frameborder="0" allowFullScreen>
            </iframe>
        </div>
    @endif
@else
    <img src="{{getFirstImageOrPlaceholder($block, 960, 720)}}" class="img-responsive" style="width: 100%">
@endif
<div style="color: {{seasonColor($block->get('text-color'))}};">
    {{$block->get('explanation')}}
</div>
