<?php
$footer = CoCo::getPage(9);
?>

<footer class="footer-type-2" style="background: {{seasonColors()->get('primary') }}; color: {{seasonColors()->get('blank') }};">
    <div class="row">
        <div class="col col-md-3 col-xs-12 col-sm-12 footer-type-2__contact">
            <h1 class="footer-type-2__header">
                Contact
            </h1>
            <article class="col col-md-12">
                <a style="color: {{seasonColors()->get('blank') }};" href="https://www.google.nl/maps/place/{{ $footer->get('streetname') }} {{ $footer->get('housenumber') }}, {{ $footer->get('zip') }} {{ $footer->get('town') }}/" target="_blank"> {{ $footer->get('streetname') }} {{ $footer->get('housenumber') }}, {{ $footer->get('zip') }} {{ $footer->get('town') }}</a><br>
                <a style="color: {{seasonColors()->get('blank') }};" href="tel:{{ CoCo::formatNLPhoneNumber($footer->get('phone-number')) }}" onclick="ga('send', 'event', 'Footer','{{ $footer->get('phone-number') }}', 'click');"> {{ $footer->get('phone-number') }}</a><br>
                <a style="color: {{seasonColors()->get('blank') }};" href="mailto:{{ $footer->get('e-mail') }}" onclick="ga('send', 'event', 'Footer','{{ $footer->get('e-mail') }}', 'click');"> {{ $footer->get('e-mail') }}</a><br>
                <button class="button__transparent--fill">
                    <a style="color: {{seasonColors()->get('blank') }}; margin-left: -5px;" href="/contact?afspraak" onclick="ga('send', 'event', 'Footer','Afspraak maken', 'click');">Maak een afspraak<span class="fa fa-angle-right" style="position: absolute; float: right; margin-left: 10px; margin-top: 2px; font-size: 24px;"></span></a>
                </button>
            </article>
        </div>
        <div class="col col-md-3 col-xs-12 col-sm-12">
            <h1 class="footer-type-2__header">
                Openingstijden
            </h1>
            @foreach($footer->get('opening-hours') as $item => $value)
                <span class="footer-type-2__opening-time col col-md-12 col-xs-12 col-sm-12">
                    {{ $value }}
                </span>
            @endforeach
        </div>
        <div class="col col-md-3 col-xs-12 col-sm-12 footer-type-2__icons">
            <h1 class="footer-type-2__header">
                Wil je ons volgen?
            </h1>
            <a href="{{$footer->get('youtube-url')}}" style="color: white;" target="_blank" onclick="ga('send', 'event', 'Footer','YouTube', 'click');">
                <i class="fa fa-youtube fa-3x"></i>
            </a>
            <a href="{{$footer->get('facebook-url')}}" style="color: white;" target="_blank" onclick="ga('send', 'event', 'Footer','Facebook', 'click');">
                <i class="fa fa-facebook fa-3x"></i>
            </a>
            <a href="{{$footer->get('instagram-url')}}" style="color: white;" target="_blank" onclick="ga('send', 'event', 'Footer','Instagram', 'click');">
                <i class="fa fa-instagram fa-3x"></i>
            </a>
            <a href="{{$footer->get('spotify-url')}}" style="color: white;" target="_blank" onclick="ga('send', 'event', 'Footer','Spotify', 'click');">
                <i class="fa fa-spotify fa-3x"></i>
            </a>
        </div>
        <div class="col col-md-2 col-xs-12 col-sm-12">
            <img class="img-responsive footer-type-2__logo" src="{{ asset('/theme/' . $template . '/img/logo-white.svg') }}" alt="{{ CoCo::getSetting('site-name', env('APP_NAME', 'CoCo')) }} Logo">
        </div>
    </div>
</footer>
@push('scripts')
    <script>
        $('#form').submit(function () {
            var email = $('#email').val();
            return true;
        });
    </script>
@endpush
