<div class="row">
    @if(CoCo::trackingAllowed())
        @foreach(CoCo::getInstagramFeed() as $index => $post)
            <div class="col-sm-4">
                <a class="thumbnail instagram-post" href="{{ $post->link }}" target="_blank" data-aos="fade-down-right"
                   data-aos-easing="ease-in-side"
                   onclick="ga('send', 'event', 'Instagram','{{ $post->link }}', 'click');">
                    <div class="instagram-image-container">
                        <img class="img-responsive" src="{{ $post->images->standard_resolution->url }}"
                             alt="@if(is_object($post->caption)){{ str_limit($post->caption->text, 110) }} @endif">
                    </div>
                    <div class="caption">
                        @if(is_object($post->caption)){{ str_limit($post->caption->text, 110) }} @endif
                    </div>
                    <div class="instagram-post-like-count">
                        <i class="fa fa-heart" aria-hidden="true"></i>
                        {{ $post->likes->count }}
                    </div>
                </a>
            </div>
            @if ($index == 17)
                @break
            @endif
        @endforeach
    @endif
</div>