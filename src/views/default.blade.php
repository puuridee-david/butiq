@extends(CoCo::getSetting('app.template_package') . '::layouts.default', ['fluidContainer' => true, 'hideBreadcrumbs' => false])
@include(CoCo::getSetting('app.template_package') . '::global-site-scripts')

@section('content')
    <main class="row">
        @if ($page->content->count() > 0)
            @foreach ($page->content as $index => $block)
                @include($block->view, [
                    'block' => $block
                ])
            @endforeach
        @endif
    </main>
@endsection
