<style>
    nav .fa-angle-down {
        color: {{seasonColor("Secundaire kleur")}}!important;
    }
    .usp {
        background: {{seasonColor("Secundaire kleur")}}!important;
    }
    .nav__list > li a:hover {
        background: none;
        color: {{seasonColor("Secundaire kleur")}}!important;

        transition: all .2s ease-in-out;
    }
    .submenu-angle {
        color: {{seasonColor("Secundaire kleur")}}!important;
    }
    .active > a {
        color: {{seasonColor("Secundaire kleur")}} !important;
    }

    .active > a:active .nav__list-item{
        background: white;
    }

    ul.dropdown-menu > li a {
        color: black;
    }

    ul.dropdown-menu > li a .fa-angle-right{
        color: {{seasonColor("Secundaire kleur")}} !important;
    }

    @media screen and (min-width: 1299px) {
        ul.dropdown-menu > li a {
            color: black;
        }
    }
</style>

<nav class="navbar nav">
    <div class="usp">
        <?php
        $usp = CoCo::getPage(24);
        ?>
        @foreach($usp->get('usp-items') as $item => $value)
            <span class="usp__item">
                <i class="fa fa-check usp__icon"></i>
                {{ $value }}
            </span>
        @endforeach
        <a href="tel:{{ CoCo::formatNLPhoneNumber(CoCo::getSetting('site_phone')) }}" class="usp__icon usp__phone">
            <span class="fa fa-phone"></span> {{ CoCo::getSetting('site_phone') }}
        </a>
    </div>
    <div class="container-fluid" style="padding-bottom: 20px;">
        <div class="navbar-header">
            <a class="navbar__brand" href="/">
                <span class="sr-only" itemprop="name">{{ CoCo::getSetting('site-name', env('APP_NAME', 'CoCo')) }}</span>
                <img class="img-responsive logo" src="{{ asset('/theme/' . $template . '/img/logo.svg') }}" alt="{{ CoCo::getSetting('site-name', env('APP_NAME', 'CoCo')) }} Logo">
            </a>
            <button type="button" class="navbar-toggle collapsed navbar__button navbar__button--menu" data-toggle="collapse" data-target="#navbar-main" aria-expanded="true">
                <span class="sr-only">{{ _('Hoofdmenu') }}</span>
                <div id="nav-icon-toggle--open" class="show">
                    <span class="icon-bar" style="background: {{seasonColors()->get('primary')}}"></span>
                    <span class="icon-bar" style="background: {{seasonColors()->get('primary')}};"></span>
                    <span class="icon-bar" style="background: {{seasonColors()->get('primary')}};"></span>
                </div>
                <div id="nav-icon-toggle--close" class="no-show">
                    <span class="fa fa-times fa-2x" style="color: {{seasonColors()->get('primary')}}; margin-left: 10px;"></span>
                </div>
                <span> Menu </span>
            </button>
            <a href="tel:{{ CoCo::formatNLPhoneNumber(CoCo::getSetting('site_phone')) }}" class="navbar__button navbar__button--phone">
                <span class="icon-bar fa fa-phone fa-2x" style="color: {{seasonColors()->get('primary')}};"></span><br>
                <span class="icon-bar">Bellen</span>
            </a>
        </div>

        <div class="collapse navbar-collapse" id="navbar-main">
            <ul class="nav nav__list">
                @include ('butiq::layouts.partials.navbar-list', [
                    'items' => isset($items) ? $items : PuurIDee\cms\models\Page::getTreeBase(),
                    'enableDropdown' => isset($enableDropdown) ? $enableDropdown : true,
                    'clickableDropdownParent' => isset($clickableDropdownParent) ? $clickableDropdownParent : true,
                    'excludeDropdownChildrenFromParentIds' => isset($excludeDropdownChildrenFromParentIds) ? $excludeDropdownChildrenFromParentIds : [],
                ])
            </ul>
        </div>

    </div>
</nav>
<div class="navigation__spacer">

</div>