@foreach ($items as $key => $item)
    {{-- check active --}}
    @php($activePage = ($item->id == $page->id))
    @php($activeGroup = (in_array($item->id, $page->path->pluck('id')->toArray())))

    {{-- nav item --}}

    @if (isset($enableDropdown) && $enableDropdown === true && $item->onlyMenuChildren->count() > 0 && !in_array($item->id, $excludeDropdownChildrenFromParentIds))
        <li class="dropdown{{ ($activePage || $activeGroup ? ' active' : '') }}">
            <a
                    href="{{ $item->permalink }}"
                    aria-haspopup="true"
                    aria-expanded="false"
                    @if(!isset($clickableDropdownParent) && !$clickableDropdownParent)
                    class="dropdown-toggle"
                    data-toggle="dropdown"
                    @endif>
                {{ $item->get('name') }}
                @if ($activePage)
                    <span class="sr-only">({{ _('huidige pagina') }})</span>
                @endif
                <span class="fa fa-angle-down fa-2x caret__dropdown"></span>
            </a>
            <span class="fa fa-angle-right fa-2x caret__dropdown"></span>
            <ul class="dropdown-menu">
                @foreach ($item->onlyMenuChildren as $child)
                    <li class="nav__list-item">
                        <a class="nav__link @if ($activePage) active @endif" href="{{ $child->permalink }}" target="_top">
                            <span class="fa fa-angle-right fa-2x submenu-angle"></span>
                            {{ $child->get('name') }}
                            @if ($activePage)
                                <span class="sr-only">({{ _('huidige pagina') }})</span>
                            @endif
                        </a>
                    </li>
                @endforeach
            </ul>
        </li>
    @else
        <li{{ ($activePage || $activeGroup ? ' class=active' : '') }}>
            <a href="{{ $item->permalink }}" target="_top">
                {{ $item->get('name') }}
                @if ($activePage)
                    <span class="sr-only">({{ _('huidige pagina') }})</span>
                @endif
            </a>
        </li>
    @endif
@endforeach