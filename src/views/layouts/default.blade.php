<!DOCTYPE html>
<html lang="{{ Config::get('app.locale') }}">

@push('stylesheets')
    <!--
    /**
     * @license
     * MyFonts Webfont Build ID 3521999, 2018-02-06T08:42:08-0500
     *
     * The fonts listed in this notice are subject to the End User License
     * Agreement(s) entered into by the website owner. All other parties are
     * explicitly restricted from using the Licensed Webfonts(s).
     *
     * You may obtain a valid license at the URLs below.
     *
     * Webfont: Mic32New-Bold by moretype
     * URL: https://www.myfonts.com/fonts/moretype/mic-32-new/bold/
     *
     * Webfont: Mic32New-Regular by moretype
     * URL: https://www.myfonts.com/fonts/moretype/mic-32-new/regular/
     *
     * Webfont: Mic32New-Light by moretype
     * URL: https://www.myfonts.com/fonts/moretype/mic-32-new/light/
     *
     *
     * License: https://www.myfonts.com/viewlicense?type=web&buildid=3521999
     * Licensed pageviews: 10,000
     * Webfonts copyright: Copyright (c) 2008 by Moretype. All rights reserved.
     *
     * � 2018 MyFonts Inc
    */

    -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/theme/' . $template . '/css/MyFontsWebfontsKit.css') }}">
    <link rel="stylesheet" href="{{ asset('/theme/' . $template . '/css/slick.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/theme/' . $template . '/css/slick-theme.css') }}" type="text/css">
    <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
@endpush
@include('coco::shared.htmlhead')

<body>

@include('butiq::layouts.partials.navbar', [
                    'items' => PuurIDee\cms\models\Page::getTreeBase(),
                    'enableDropdown' => true,
                    'clickableDropdownParent' => true,
                    'excludeDropdownChildrenFromParentIds' => [],
                ])

<div class="{{ (isset($fluidContainer) ? 'container-fluid' : 'container') }}">
    <div class="content">
        @yield('content')
    </div>
</div>

@include('butiq::components.footer-type-2')

@cocoScripts(bootstrap)
<script type="text/javascript" src="{{ asset('/theme/' . $template . '/js/slick.min.js') }}"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>


{{-- scripts from views --}}
@push('scripts')
    <script>
        AOS.init();

        $('#nav-icon-toggle--close').click(function () {
            $('#navbar-main').removeClass('open').addClass('collapsed');
            $('.navbar-collapse').attr('style', 'display:none!important;');
            $('#nav-icon-toggle--close').removeClass('show').addClass('no-show');
            $('#nav-icon-toggle--open').removeClass('no-show').addClass('show');
            console.log('click');
        });
        $('#nav-icon-toggle--open').click(function () {
            $('#navbar-main').removeClass('collapsed').addClass('open');
            $('.navbar-collapse').attr('style', 'display:block!important;');
            $('#nav-icon-toggle--open').removeClass('show').addClass('no-show');
            $('#nav-icon-toggle--close').removeClass('no-show').addClass('show');
            console.log('click');
        });

        var didScroll;
        var lastScrollTop = 0;
        var delta = 5;
        var navbarHeight = 140;

        $(window).scroll(function (event) {
            didScroll = true;
        });

        setInterval(function () {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);

        function hasScrolled() {
            var st = $(this).scrollTop();

            if (Math.abs(lastScrollTop - st) <= delta)
                return;

            if (st > lastScrollTop && st > navbarHeight) {
                $('.navbar').removeClass('nav-down').addClass('nav-up');
            } else {
                if (st + $(window).height() < $(document).height()) {
                    $('.navbar').removeClass('nav-up').addClass('nav-down');
                }
            }

            lastScrollTop = st;
        }
    </script>
@endpush
@stack('scripts')

</body>
</html>
