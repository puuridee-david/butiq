<?php

if (!function_exists('seasonColors')) {

    function seasonColors()
    {
        $colors = new \Illuminate\Database\Eloquent\Collection();
        $season = CoCo::getPage(23);
        $active_season = $season->get('active-season');

        if ($active_season == "Automatisch") {
            $active_season = getAutomatedSeason();
        }
        if ($active_season == "Zomer") {
            $colors = collect([
                "primary" => $season->get('summer-color-1'),
                "primary-tint" => $season->get('summer-color-2'),
                "secondary" => $season->get('summer-color-3'),
                "blank" => $season->get('summer-color-4'),
            ]);
        }
        if ($active_season == "Herfst") {
            $colors = collect([
                "primary" => $season->get('autumn-color-1'),
                "primary-tint" => $season->get('autumn-color-2'),
                "secondary" => $season->get('autumn-color-3'),
                "blank" => $season->get('autumn-color-4'),
            ]);
        }
        if ($active_season == "Winter") {
            $colors = collect([
                "primary" => $season->get('winter-color-1'),
                "primary-tint" => $season->get('winter-color-2'),
                "secondary" => $season->get('winter-color-3'),
                "blank" => $season->get('winter-color-4'),
            ]);
        }
        if ($active_season == "Lente") {
            $colors = collect([
                "primary" => $season->get('spring-color-1'),
                "primary-tint" => $season->get('spring-color-2'),
                "secondary" => $season->get('spring-color-3'),
                "blank" => $season->get('spring-color-4'),
            ]);
        }
        return $colors;
    }
}
if (!function_exists('seasonColor')) {
    function seasonColor($color)
    {
        $season = CoCo::getPage(23);
        $active_season = $season->get('active-season');

        if ($color == "Zwart") {
            return "#000";
        } else if ($color == "Wit") {
            return "#FFF";
        }

        if ($active_season == "Automatisch") {
            $active_season = getAutomatedSeason();
        }

        if ($active_season == "Lente") {
            if ($color == "Primaire kleur") {
                return $season->get('spring-color-1');
            } else if ($color == "Primaire tint") {
                return $season->get('spring-color-2');
            } else if ($color == "Secundaire kleur") {
                return $season->get('spring-color-3');
            } else if ($color == "Blanco") {
                return $season->get('spring-color-4');
            }
        }
        if ($active_season == "Zomer") {
            if ($color == "Primaire kleur") {
                return $season->get('summer-color-1');
            } else if ($color == "Primaire tint") {
                return $season->get('summer-color-2');
            } else if ($color == "Secundaire kleur") {
                return $season->get('summer-color-3');
            } else if ($color == "Blanco") {
                return $season->get('summer-color-4');
            }
        }
        if ($active_season == "Herfst") {
            if ($color == "Primaire kleur") {
                return $season->get('autumn-color-1');
            } else if ($color == "Primaire tint") {
                return $season->get('autumn-color-2');
            } else if ($color == "Secundaire kleur") {
                return $season->get('autumn-color-3');
            } else if ($color == "Blanco") {
                return $season->get('autumn-color-4');
            }
        }
        if ($active_season == "Winter") {
            if ($color == "Primaire kleur") {
                return $season->get('winter-color-1');
            } else if ($color == "Primaire tint") {
                return $season->get('winter-color-2');
            } else if ($color == "Secundaire kleur") {
                return $season->get('winter-color-3');
            } else if ($color == "Blanco") {
                return $season->get('winter-color-4');
            }
        }
    }
}
if (!function_exists('blockAlignmentClass')) {
    function blockAlignmentClass($align)
    {
        if ($align->get('block-align') == "Afbeelding links, tekst rechts") {
            return "media-left";
        } else {
            return "media-right";
        }
    }
}
if (!function_exists('blockAlignment')) {
    function blockAlignment($align)
    {
        if ($align->get('block-align') == "Afbeelding links, tekst rechts") {
            return "media-left";
        } else {
            return "media-right";
        }
    }
}
if (!function_exists('setColor')) {
    function setColor($color)
    {
        switch ($color) {
            case 'Transparant':
                return "transparant";
                break;
            case 'Wit':
                return "#FFF";
                break;
            case 'Blauw':
                return "#20cbd4";
                break;
            case 'Licht grijs':
                return "#FAFAFA";
                break;
            case 'Zwart':
                return "#000";
                break;
            default:
                return "#000";
                break;
        }
    }
}
if (!function_exists('getMargin')) {
    function getMargin($marging)
    {
        return str_replace("%", "vh", $marging);
    }
}
if (!function_exists('setHeight')) {
    function setHeight($height)
    {
        return str_replace("%", "vh", $height);
    }
}
if (!function_exists('setWidth')) {
    function setWidth($width)
    {
        return str_replace("%", "vw", $width);
    }
}
if (!function_exists('setAlignment')) {
    function setAlignment($alignment)
    {
        switch ($alignment) {
            case 'Links':
                return "left";
                break;
            case 'Rechts':
                return "right";
                break;
            case 'Gecentreerd':
                return "center";
                break;
            case 'Gevuld':
                return "justify";
                break;
            default:
                return "left";
                break;
        }
    }
}
if (!function_exists('getRandomFallbackImage')) {
    function getRandomFallbackImage()
    {
        $images = [
            'placeholder.jpg',
        ];

        return asset("img/" . array_random($images));
    }
}
if (!function_exists('getLinkTarget')) {
    function getLinkTarget($target)
    {
        switch ($target) {
            case 'Nee':
                return "_self";
                break;
            case 'Ja':
                return "_blank";
                break;
            default:
                return "_self";
                break;
        }
    }
}
if (!function_exists('getFirstImageOrPlaceholder')) {
    function getFirstImageOrPlaceholder($object, $width = null, $height = null)
    {
        if (!$width || !$height) {
            return $object->media->count() ? $object->media->first()->thumbnail() : asset('img/placeholder.png');
        } else {
            return $object->media->count() ? $object->media->first()->thumbnail($width, $height) : asset('img/placeholder.png');
        }
    }
}
if (!function_exists('getFirstImageOrSquarePlaceholder')) {
    function getFirstImageOrSquarePlaceholder($object, $width = null, $height = null)
    {
        if (!$width || !$height) {
            return $object->media->count() ? $object->media->first()->thumbnail() : asset('img/placeholder-square.png');
        } else {
            return $object->media->count() ? $object->media->first()->thumbnail($width, $height) : asset('img/placeholder-square.png');
        }

    }
}
if (!function_exists('getSeasonImageOrDefault')) {
    function getSeasonImageOrDefault($object, $width = null, $height = null)
    {
        $season = CoCo::getPage(23);
        $active_season = $season->get('active-season');

        if ($active_season == "Automatisch") {
            $active_season = getAutomatedSeason();
        }

        switch ($active_season) {
            case 'Lente':
                if (!$width || !$height) {
                    return $object->media->count() ? $object->media->first()->thumbnail() : asset('img/placeholder.png');
                } else {
                    return $object->media->count() ? $object->media->first()->thumbnail($width, $height) : asset('img/placeholder.png');
                }
                break;
            case 'Zomer':
                if ($object->media->count() == 2 || $object->media->count() > 2) {
                    if (!$width || !$height) {
                        return $object->media->count() ? $object->media[1]->thumbnail() : asset('img/placeholder.png');
                    } else {
                        return $object->media->count() ? $object->media[1]->thumbnail($width, $height) : asset('img/placeholder.png');
                    }
                } else {
                    if (!$width || !$height) {
                        return $object->media->count() ? $object->media->first()->thumbnail() : asset('img/placeholder.png');
                    } else {
                        return $object->media->count() ? $object->media->first()->thumbnail($width, $height) : asset('img/placeholder.png');
                    }
                }
                break;
            case 'Herfst':
                if ($object->media->count() == 3 || $object->media->count() > 3) {
                    if (!$width || !$height) {
                        return $object->media->count() ? $object->media[2]->thumbnail() : asset('img/placeholder.png');
                    } else {
                        return $object->media->count() ? $object->media[2]->thumbnail($width, $height) : asset('img/placeholder.png');
                    }
                } else {
                    if (!$width || !$height) {
                        return $object->media->count() ? $object->media->first()->thumbnail() : asset('img/placeholder.png');
                    } else {
                        return $object->media->count() ? $object->media->first()->thumbnail($width, $height) : asset('img/placeholder.png');
                    }
                }
                break;
            case 'Winter':
                if ($object->media->count() == 4 || $object->media->count() > 4) {
                    if (!$width || !$height) {
                        return $object->media->count() ? $object->media[3]->thumbnail() : asset('img/placeholder.png');
                    } else {
                        return $object->media->count() ? $object->media[3]->thumbnail($width, $height) : asset('img/placeholder.png');
                    }
                } else {
                    if (!$width || !$height) {
                        return $object->media->count() ? $object->media->first()->thumbnail() : asset('img/placeholder.png');
                    } else {
                        return $object->media->count() ? $object->media->first()->thumbnail($width, $height) : asset('img/placeholder.png');
                    }
                }
                break;
            default:
                if (!$width || !$height) {
                    return $object->media->count() ? $object->media->first()->thumbnail() : asset('img/placeholder.png');
                } else {
                    return $object->media->count() ? $object->media->first()->thumbnail($width, $height) : asset('img/placeholder.png');
                }
                break;
        }
    }
}
if (!function_exists('getAutomatedSeason')) {
    function getAutomatedSeason()
    {
        $date = Carbon\Carbon::createFromDate(null, null, null);
        $springBegin = Carbon\Carbon::createFromDate(null, 3, 20);
        $summerBegin = Carbon\Carbon::createFromDate(null, 6, 21);
        $fallBegin = Carbon\Carbon::createFromDate(null, 9, 22);
        $winterBegin = Carbon\Carbon::createFromDate(null, 12, 21);
        switch (true) {
            case $date->between($springBegin, $summerBegin):
                return "Lente";
                break;
            case $date->between($summerBegin, $fallBegin):
                return "Zomer";
                break;
            case $date->between($fallBegin, $winterBegin):
                return "Herfst";
                break;
            case $date->between($winterBegin->subYear(), $springBegin):
                return "Winter";
                break;
            case $date->between($winterBegin->addYear(), $springBegin):
                return "Winter";
                break;
            default:
                return "Lente";
                break;
        }
    }
}
?>