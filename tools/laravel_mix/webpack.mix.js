/**
 * laravel mix config template
 * usage:
 *     - place file in project root directory and run 'npm install' once and 'npm run watch' while editing template sass files, to minimize css use 'npm run production'
 *
 * @see        https://laravel.com/docs/5.5/mix
 *
 */

let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('vendor/coco/boilerplate-template/src/sass/main.scss', 'public/css/screen.css');
